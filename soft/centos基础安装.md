#安装基础环境

##1. 安装基础编译环境

	yum groupinstall "Development tools"
	yum install zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel


##2. 安装gcc

###2.1. 步骤：

	sudo yum install glibc-static libstdc++-static
	
	wget http://ftp.gnu.org/gnu/gcc/gcc-6.1.0/gcc-6.1.0.tar.gz
	
	tar xvf gcc-6.1.0.tar.gz
	
	cd gcc-6.1.0
	
	./contrib/download_prerequisites
	
	cd ..
	
	mkdir build_gcc6.1
	# 创建临时文件，编译
	
	cd build_gcc6.1
	
	../gcc-6.1.0/configure --enable-checking=release --enable-languages=c,c++ --disable-multilib
	
	make -j4
	
	sudo make install
	
	
###2.2. 坑纪录 

#### 坑1，依赖包下载

很多文章都说自己去下载mpr等等三个库的源码，自己编译，这是相当不理智的，浪费了不少时间，因为我们自己去下载包括安装都有可能碰到版本问题，以及路径问题，好好的方案还是

使用./contrib/download_prerequisites  ,他会自动下载

#### 坑2，GLIBCXX找不到

源码编译升级安装了gcc后，编译程序或运行其它程序时，有时会出现类似/usr/lib64/libstdc++.so.6: version `GLIBCXX_3.4.21' not found的问题。这是因为升级gcc时，生成的动态库没有替换老版本gcc的动态库导致的，将gcc最新版本的动态库替换系统中老版本的动态库即可解决

`strings /usr/lib64/libstdc++.so.6 | grep GLIBC`

**解决:修改系统默认动态库的指向，即：重建默认库的软连接。**

找到最新的，6.1对应的是libstdc++.so.6.0.22,`find / -name "libstdc++.so*"`

切换工作目录至/usr/lib64：`cd /usr/lib64`

删除原来软连接：`rm -rf libstdc++.so.6`

将默认库的软连接指向最新动态库：`ln -s libstdc++.so.6.0.22 libstdc++.so.6`