#安装supervisord

#1. 安装

可以通过 yum 或 pip 方式安装

	sudo yum install supervisor
	# or
	sudo pip install supervisor

#2. 配置
##2.1 基础配置


	# 配置文件模板
	echo_supervisord_conf > supervisord.conf
	sudo cp supervisord.conf /etc/supervisord.conf
	# 各个应用配置放在此处
	sudo mkdir /etc/supervisord.d/
	# 编辑配置
	sudo vi /etc/supervisord.conf
	
	
	# 在文件末端修改或添加
	[include]
	files = /etc/supervisord.d/*.conf

##2.2 服务配置

创建 service 脚本 sudo vi /etc/rc.d/init.d/supervisord

	#!/bin/sh
	#
	# /etc/rc.d/init.d/supervisord
	#
	# Supervisor is a client/server system that
	# allows its users to monitor and control a
	# number of processes on UNIX-like operating
	# systems.
	#
	# chkconfig: - 64 36
	# description: Supervisor Server
	# processname: supervisord
	
	
	# Source init functions
	. /etc/rc.d/init.d/functions
	
	
	prog="supervisord"
	
	
	prefix="/usr/"
	exec_prefix="${prefix}"
	prog_bin="${exec_prefix}/bin/supervisord"
	PIDFILE="/var/run/$prog.pid"
	
	
	start()
	{
	      echo -n $"Starting $prog: "
	      daemon $prog_bin --pidfile $PIDFILE
	      [ -f $PIDFILE ] && success $"$prog startup" || failure $"$prog startup"
	      echo
	}
	
	
	stop()
	{
	      echo -n $"Shutting down $prog: "
	      [ -f $PIDFILE ] && killproc $prog || success $"$prog shutdown"
	      echo
	}
	
	
	case "$1" in
	
	
	start)
	  start
	;;
	
	
	stop)
	  stop
	;;
	
	
	status)
	      status $prog
	;;
	
	
	restart)
	  stop
	  start
	;;
	
	
	*)
	  echo "Usage: $0 {start|stop|restart|status}"
	;;
	
	
	esac

添加自启动：

	sudo chmod +x /etc/rc.d/init.d/supervisord
	sudo chkconfig --add supervisord
	sudo chkconfig supervisord on
	sudo service supervisord start

4. 添加应用

创建应用配置文件，sudo vi /etc/supervisord.d/myapp.conf，基础配置:
	
	[program:myapp]
	command=/usr/bin/gunicorn -w 1 wsgiapp:application ;启动命令
	directory=/srv/www                                ;程序目录
	autostart=true                                    ;自启动
	autorestart=true                                  ;启动重启
	startsecs=10
	startretries=3
	logfile=/var/log/supervisor.log

重启服务查看自己的应用是否启动，非自启动应用通过 sudo supervisorctl start myapp 启动

