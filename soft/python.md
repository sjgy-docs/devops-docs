#安装python

##1. 安装python2.\* & python3.\*
### python2.*

	# Python 2.7.6:
	wget http://python.org/ftp/python/2.7.6/Python-2.7.6.tar.xz
	tar xf Python-2.7.6.tar.xz
	cd Python-2.7.6
	./configure --prefix=/usr/local --enable-unicode=ucs4 --enable-shared LDFLAGS="-Wl,-rpath /usr/local/lib"
	make && make altinstall
	
### python3.*
	
	# Python 3.3.5:
	wget http://python.org/ftp/python/3.3.5/Python-3.3.5.tar.xz
	tar xf Python-3.3.5.tar.xz
	cd Python-3.3.5
	./configure --prefix=/usr/local --enable-shared LDFLAGS="-Wl,-rpath /usr/local/lib"
	make && make altinstall
	
##2. 安装setuptools & pip

	# First get the setup script for Setuptools:
	wget https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py
	 
	# Then install it for Python 2.7 and/or Python 3.3:
	python2.7 ez_setup.py
	python3.3 ez_setup.py
	 
	# Now install pip using the newly installed setuptools:
	easy_install-2.7 pip
	easy_install-3.3 pip
	 
	# With pip installed you can now do things like this:
	pip2.7 install [packagename]
	pip2.7 install --upgrade [packagename]
	pip2.7 uninstall [packagename]
	
##3. 建立隔离环境

###python2.* 使用`virtualenv`

	# 安装virtualenv:
	pip2.7 install virtualenv
	
	# 建立沙箱
	virtualenv projectenv
	 
	# 启动沙箱:
	source projectenv/bin/activate
	
	# 关闭沙箱
	deactivate
	 
###python3.* 使用`pyvenv`	 

	# 使用 Python 3.3 自带的 pyvenv 命令 建立沙箱环境:
	pyvenv projectenv

	# 启动沙箱:
	source projectenv/bin/activate
	
	# 关闭沙箱
	deactivate