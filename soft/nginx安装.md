#Nginx安装

##介绍

将Nginx作为反向代理，中转客户端资源请求到适当的服务器。

一些网络安装使用代理服务器过滤或缓存本地网络机器到Internet的HTTP请求。因为我们将运行一些在不同TCP端口上的Tornado实例，因此我们将使用反向代理服务器：客户端通过Internet连接一个反向代理服务器，然后反向代理服务器发送请求到代理后端的Tornado服务器池中的任何一个主机。代理服务器被设置为对客户端透明的，但它会向上游的Tornado节点传递一些有用信息，比如原始客户端IP地址和TCP格式。

我们的服务器配置如图所示。反向代理接收所有传入的HTTP请求，然后把它们分配给独立的Tornado实例。


##安装
###1. 在nginx官方网站下载一个rpm包，下载地址是：http://nginx.org/en/download.html

	wget http://nginx.org/packages/centos/6/noarch/RPMS/nginx-release-centos-6-0.el6.ngx.noarch.rpm

###2. 安装rpm包,添加yum软件源

	rpm -ivh nginx-release-centos-6-0.el6.ngx.noarch.rpm 

###3. 开始正式安装nginx

	yum install －y nginx

###4. nginx的几个默认目录

	whereis nginx
	nginx: /usr/sbin/nginx /etc/nginx /usr/share/nginx

1. 配置所在目录：/etc/nginx/
2. PID目录：/var/run/nginx.pid
3. 错误日志：/var/log/nginx/error.log
4. 访问日志：/var/log/nginx/access.log
5. 默认站点目录：/usr/share/nginx/html

###5. 常用命令

1. 启动nginx：nginx
2. 重启nginx：killall -HUP nginx
3. 测试nginx配置：nginx -t

###6. 无法站外访问？

####6.1 设置80端口

如果是以上的故障现象，很可能是被CentOS的防火墙把80端口拦住了，尝试执行以下命令，打开80端口：

`iptables -I INPUT -p tcp --dport 80 -j ACCEPT`

然后用：

`/etc/init.d/iptables status`

查看当前的防火墙规则，如果发现有这样一条：

`ACCEPT     tcp  --  0.0.0.0/0            0.0.0.0/0           tcp dpt:80`

####6.2 关闭SElinux

	/usr/bin/setenforce 修改SELinux的实时运行模式
	setenforce 1 设置SELinux 成为enforcing模式
	setenforce 0 设置SELinux 成为permissive模式
	
如果要彻底禁用SELinux 需要在/etc/sysconfig/selinux中设置参数selinux=0,或者在/etc/grub.conf中添加这个参数


通过`/usr/bin/setstatus -v`查看状态

