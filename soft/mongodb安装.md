#mongodb安装

##1. 下载最新

[下载中心](https://www.mongodb.com/download-center#community)目前最新

[https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-3.2.7.tgz](https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-3.2.7.tgz)

##2. 安装

软件一般安装到`/usr/local`

	tar zxvf mongodb-linux-x86_64-3.2.7.tgz
	mv mongodb-linux-x86_64-3.2.7 /usr/local/mongodb
	cd mongodb
	mkdir db
	mkdir logs
	cd bin

##3. 配置

打开

	vi mongodb.conf

配置
	
	#指定数据存储位置
	dbpath=/usr/local/mongodb/db
	
	#指定日志文件生成位置	
	logpath=/usr/local/mongodb/logs/mongodb.log

	port=27017
	fork=true
	nohttpinterface=true

###3.1 重新绑定mongodb配置文件和访问ip

绑定到localhost,不对外网开放	

	/usr/local/mongodb/bin/mongod --bind_ip localhost -f /usr/local/mongodb/bin/mongodb.conf

###3.2 开机自动启动mongodb

打开编辑

	vi /etc/rc.d/rc.local

加入

	/usr/local/mongodb/bin/mongod --config /usr/local/mongodb/bin/mongodb.conf

###3.3 测试

	#进入mongodb的shell模式 /usr/local/mongodb/bin/mongo
	#查看数据库列表
	show dbs
	#当前db版本
	db.version();