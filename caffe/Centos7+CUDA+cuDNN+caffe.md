#Centos7+CUDA+cuDNN+caffe

>想了解更多请下载[具体安装文档](http://developer.download.nvidia.com/compute/cuda/8.0/secure/prod/docs/sidebar/CUDA_Installation_Guide_Linux.pdf?autho=1477981438_a36bc5539867d5d089ec1c97fb8452fa&file=CUDA_Installation_Guide_Linux.pdf)

##1. 文档环境
- 显卡：Telsa k80
- 系统：Centos7.2

###1.1 准备

####1.1.1 安装EPEL源
>[EPEL(Extra Packages for Enterprise Linux)](https://fedoraproject.org/wiki/EPEL)，为“红帽系”的操作系统提供额外的软件包，适用于RHEL、CentOS和Scientific Linux.

[官网链接](https://admin.fedoraproject.org/mirrormanager/EPEL/)有很多镜像，这使用的是[aliyun的YUM源](http://mirrors.aliyun.com/centos/)

####1.1.2 更新系统

	yum update -y

##2. 显卡安装

###2.1 下载安装文件
从[Nvidia官网](www.nvidia.cn)下载，参数诸元如图：

![Alt text](1477977129903.png)

###2.2 安装系统内核
因为驱动编译需要依赖系统内核模块，所以必须安装内核相关的包，以及编译源码时需要的工具包：

	yum install gcc gcc-c++ -y
	yum install kernel-devel-$(uname -r) kernel-headers-$(uname -r) -y

###2.3 禁用第三方开源驱动
Centos一般默认安装了默认安装了nouveau，一个第三方开源的nvidia驱动。所以必须把驱动加入黑名单。

打开`/lib/modprobe.d/dist-blacklist.conf`

	//将nvidiafb注释掉。
	#blacklist nvidiafb

	//然后添加以下语句：
	blacklist nouveau
	options nouveau modeset=0

###2.4 重建initramfs image

	mv /boot/initramfs-$(uname -r).img /boot/initramfs-$(uname -r).img.bak
	dracut /boot/initramfs-$(uname -r).img $(uname -r) --force

###2.4 图形界面(可选)
如果Centos附带了图形界面，那么需要修改运行级别为文本模式：

	systemctl set-default multi-user.target

###2.5 检查并安装驱动

重启系统

	reboot now

重新进入，检查nouveau driver是否加载，如果成功禁用，应该什么消息都不返还：

	lsmod | grep nouveau

安装驱动

	./NVIDIA-XXXX.run --kernel-source-path=/usr/src/kernels/$(uname -r)

测试

	nvidia-smi

应该会出现这个画面
![Alt text](./1478068372786.png)

##3. 安装CUDA

###3.1 安装RPM源
从[官网](developer.nvidia.com)下载rpm源，如图

![Alt text](1477980737508.png)

###3.2 测试

	#把范例拷贝到root根路径下
	/usr/local/cuda/bin/cuda-install-samples-8.0.sh /root
	cd /root/NVIDIA_CUDA-8.0_Samples
	#进入任意一个子文件夹,本文进入1_Utilities/deviceQuery
	cd 1_Utilities/deviceQuery
	#编译文件并执行
	make & ./deviceQuery

应该会打印出CUDA使用的GPU信息

![Alt text](./1478067748032.png)

##4. 安装Caffe

###4.1 安装依赖包

	yum install atlas-devel snappy-devel boost-devel leveldb leveldb-devel hdf5 hdf5-devel  glog glog-devel gflags gflags-devel protobuf protobuf-devel opencv opencv-devel lmdb lmdb-devel

###4.2 安装Caffe

	git clone git@github.com:BVLC/caffe.git
 	cd caffe
	cp Makefile.config.example Makefile.config
	vim Makefile
	# 将 LIBRARIES += cblas atlas 改为 LIBRARIES += satlas tatlas
 	vim Makefile.config
	# 修改内容为：
	# 在LIBRARY_DIRS后加上 /usr/lib64/atlas
	make all

###4.3 添加动态链

	vim /etc/ld.so.conf.d/caffe.conf
	#添加如下字段,注意换行
	/usr/local/cuda/lib64
	/usr/local/lib64
	/usr/local/lib
	ldconfig

###4.4 测试

重启并获取mnist训练集

	reboot
	./data/mnist/get_mnist.sh
	./examples/mnist/create_mnist.sh

运行测试集，应该能跑通，但是会很慢。又及，出错可能是cuDNN没改好

	# 修改 ~/caffe/caffe-master/examples/mnist/lenet_solver.prototxt 最后一行
	# 修改最后一行: solver_mode: GPU
	time ./examples/mnist/train_lenet.sh

##5 安装cuDNN

###5.1 注册
[官网位置](https://developer.nvidia.com/cudnn)
![Alt text](1478069310352.png)

####5.1.1 下载并解压缩进入文件夹

	#将头文件导入系统头文件夹下
	cp include/* /usr/local/cuda/include/

	#将库文件导入系统库文件夹下
	cp lib64/* /usr/local/cuda/lib64/

	#重新注册
	ldconfig

####5.1.1 全局修改

将Caffe包里的Makefile.config 中 USE_CUDNN 行的注释去除，再重新编译

###5.2 测试
将4.4中的测试重来一次

	vim examples/mnist/lenet_solver.prototxt
	# 修改 ~/caffe/caffe-master/examples/mnist/lenet_solver.prototxt 最后一行
	# 修改最后一行: solver_mode: GPU
	time ./examples/mnist/train_lenet.sh